import json
import os
import random

from pandas.io.json import json_normalize

from cbr import basecbr


class simpleRecommendation:
	def __init__(self):
		pass

	def run(self, case, problem):
		allcases = self.getAllcases(case)
		cbr = basecbr(problem, allcases)
		outcome = cbr.recommend()
		return outcome

	def getAllcases(self, data):
		cases = []
		for i in range(0,len(data)//16+1):
			cases2 = []
			for j in range(2, 12):
				cases2.append((data.iloc[i*16+j, 1], data.iloc[i*16+j, 2]))
			cases.append(cases2)
		return cases

	def feedback(self,feedbackvalue):
		allcases = self.getAllcases()

		# 1. 모든 케이스를 가져온다.
		# 2. 모든 케이스 중 caseNum에 맞는 놈을 찾는다.
		# 3. feedbackvalue를 업데이트 한다.
		# 4. feedback이 반영되면 True 오류가 나면 False 리턴

		return True

	def readfiles(self, path="case/cases"):
		listFiles = os.listdir(path)

		for i in range(0, len(listFiles)):
			with open(path + "/" + listFiles[i]) as fileToRead:
				data = json.load(fileToRead)
				try:
					dataframe
				except NameError:
					dataframe = json_normalize(data)
				else:
					dataframe = dataframe.append(json_normalize(data), ignore_index=True)

		testCaseIndexes = sorted(random.sample(range(2335), 467))
		dataframeTest = dataframe.copy()
		for j in range (0, len(listFiles)):
			if j not in testCaseIndexes:
				dataframeTest = dataframeTest.drop(j)

		for j in range (0, len(listFiles)):
			if j in testCaseIndexes:
				dataframe = dataframe.drop(j)

		#Reindex the dataframes from 0 to (n-1)
		dataframe = dataframe.reset_index(drop=True)
		dataframeTest = dataframeTest.reset_index(drop=True)
		return [dataframe, dataframeTest]

		#Debugging
		# print(dataframeTest.shape)
		# print(dataframe.shape)


if __name__=="__main__":
	a = simpleRecommendation()
	a.readfiles()
	b = a.getAllcases()
