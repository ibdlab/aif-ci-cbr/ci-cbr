import json
import os
import random

from pandas.io.json import json_normalize


# Reads the JSON files and returns the dataset (80% training / 20% test)
def readfiles(path="case/cases"):
    listFiles = os.listdir(path)

    for i in range(0, len(listFiles)):
        with open(path + "/" + listFiles[i]) as fileToRead:
            data = json.load(fileToRead)
            try:
                dataframe
            except NameError:
                dataframe = json_normalize(data)
            else:
                dataframe = dataframe.append(json_normalize(data), ignore_index=True)

    testCaseIndexes = sorted(random.sample(range(2335), 467))
    dataframeTest = dataframe.copy()
    for j in range(0, len(listFiles)):
        if j not in testCaseIndexes:
            dataframeTest = dataframeTest.drop(j)

    for j in range(0, len(listFiles)):
        if j in testCaseIndexes:
            dataframe = dataframe.drop(j)

    # Reindex the dataframes from 0 to (n-1)
    dataframe = dataframe.reset_index(drop=True)
    dataframeTest = dataframeTest.reset_index(drop=True)
    return [dataframe, dataframeTest]

    # Debugging
    # print(dataframeTest.shape)
    # print(dataframe.shape)


def normalize_dataset(dataframe_training, dataframe_test, variables):
    dataset_variables = dict()

    # find min and max values for the variables
    for column in variables:
        values = {
            "min_value": None,
            "max_value": None
        }
        dataset_variables[column] = values

        for row in dataframe_training.itertuples(index=True):
            str_value = getattr(row, column)

            if getattr(row, column) != "None" and not isinstance(getattr(row, column), (list,)):
                value = float(str_value)
                # print(value)
                if (dataset_variables[column]["min_value"] is None and str_value != "None") or (
                        str_value != "None" and value < dataset_variables[column]["min_value"]):
                    dataset_variables[column]["min_value"] = value
                    # print("min value is now: " + str(dataset_variables[column]["min_value"]))

                if (dataset_variables[column]["max_value"] is None and str_value != "None") or (
                        str_value != "None" and value > dataset_variables[column]["max_value"]):
                    dataset_variables[column]["max_value"] = value
                    # print("max value for " + column + " is now: " + str(dataset_variables[column]["max_value"]))

    # normalize using rescaling (min-max normalization)
    for column in variables:
        for row in dataframe_training.itertuples(index=True):
            str_value = getattr(row, column)
            min_value = dataset_variables[column]["min_value"]
            max_value = dataset_variables[column]["max_value"]
            normalized_value = None
            value = None

            if getattr(row, column) != "None" and not isinstance(getattr(row, column), (list,)):
                value = float(str_value)
                normalized_value = (value - min_value) / (max_value - min_value)
                # print(normalized_value)

            if normalized_value is not None:
                dataframe_training.at[row.Index, column] = str(normalized_value)

        for row in dataframe_test.itertuples(index=True):
            str_value = getattr(row, column)
            min_value = dataset_variables[column]["min_value"]
            max_value = dataset_variables[column]["max_value"]
            normalized_value = None
            value = None

            if getattr(row, column) != "None" and not isinstance(getattr(row, column), (list,)):
                value = float(str_value)
                normalized_value = (value - min_value) / (max_value - min_value)
                # print(normalized_value)

            if normalized_value is not None:
                dataframe_test.at[row.Index, column] = str(normalized_value)

    return [dataframe_training, dataframe_test]
