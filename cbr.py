import math
import pandas as pd

class basecbr:
    def __init__(self, problem, allCases):
        self.allCases = allCases
        self.problem = problem

    def similarity_measure(self):
        similarity_list = []
        for case_num in range(len(self.allCases)):
            match = 0
            unmatch = 0
            distance = 0
            category_weight = 0
            numeric_weight = 0
            for i in range(1, len(self.problem)):
                if type(self.problem[i][1]) is float:                                                  #null 값을 float으로 인식 : 후에 price가 dollar라면 1.5$가 만약 있을시 어떻게 해야할지..
                    pass
                elif type(self.problem[i][1]) is int:
                    numeric_weight += 1/9
                    distance += (self.problem[i][1]-self.allCases[case_num][i][1])**2
                else:
                    category_weight += 1/9
                    if self.problem[i][1] == self.allCases[case_num][i][1]:
                        match += 1
                    else:
                        unmatch += 1
            similarity_list.append([self.allCases[case_num], numeric_weight*(1/(1 + math.sqrt(distance))) + category_weight*(match / (match + unmatch))])
        return similarity_list

    def recommend(self):
        similarity_list = self.similarity_measure()
        max = 0
        max_case = 0
        for i in range(len(similarity_list)):
            if similarity_list[i][1] > max:
                max = similarity_list[i][1]
                max_case = similarity_list[i][0]
        return max_case

'''
if __name__=="__main__":
    problem = []
    problem_case = pd.read_excel('problem.xlsx')
    for i in range(0, 10):
        problem.append((problem_case.iloc[i + 2, 1], problem_case.iloc[i + 2, 2]))

    xls = pd.ExcelFile('TRAVEL_TEST.xls')
    casebase = pd.read_excel(xls, 'caseset')

    a = simpleRecommendation()
    all = a.getAllcases(casebase)

    b = basecbr(problem, all)
    b.similarity_measure
'''