import pandas as pd
from recommendation import simpleRecommendation
from cbr import basecbr

class test:
    def __init__(self, caseset_data, testset_data, answer):
        recommendation = simpleRecommendation()
        self.caseset = recommendation.getAllcases(caseset_data)
        self.testset = recommendation.getAllcases(testset_data)
        self.answer = recommendation.getAllcases(answer)

    def accuracy_rate(self):
        accuracy_score = 0
        for num_pr in range(0, len(self.testset)):
            cbr = basecbr(self.testset[num_pr], self.caseset)
            outcome = cbr.recommend()
            for i in range(1, 10):
                if type(self.testset[num_pr][i][1]) is float:
                    if outcome[i][1] == self.answer[num_pr][i][1]:
                        accuracy_score += 1
                    else:
                        pass
        accuracy_rate = accuracy_score/len(self.testset)
        return accuracy_rate






'''
xls = pd.ExcelFile('TRAVEL_TEST.xls')
casebase = pd.read_excel(xls, 'caseset')
testset = pd.read_excel(xls, 'testset')
answer = pd.read_excel(xls, 'answer')
a = test(casebase, testset, answer)
b = a.getAllcases()
'''

