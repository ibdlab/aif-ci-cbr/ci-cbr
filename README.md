# CI-CBR Travel

## Requirements

*  Python3
*  Pandas
*  Numpy

1. Install Python3 on your machine if you don't have it.
2. Install Pandas running the command:
    ```c
        pip3 install pandas
    ```
3. Install Numpy running the command:
    ```c
        pip3 install numpy
    ```
